$(".header__hambuger").click(function() {
  $(".left-bar").addClass("active");
  $(".left-bar__overlay").addClass("active");
});

$(".left-bar__overlay").click(function() {
  $(".left-bar").removeClass("active");
  $(".left-bar__overlay").removeClass("active");
});

$(".category-filter__text").click(function() {
  $(".category-filter").addClass("active");
  $(".category-filter__overlay").addClass("active");
});

$(".category-filter__overlay").click(function() {
  $(".category-filter").removeClass("active");
  $(".category-filter__overlay").removeClass("active");
});

$(".main-banner__items").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000
});

$(".main-banner__link").click(function() {
  $("html, body").animate(
    {
      scrollTop: $(".new-option").offset().top
    },
    800,
    "linear"
  );
});

if (window.innerWidth < 767) {
  $(".new-option--love")
    .find(".new-option__items")
    .slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      autoplaySpeed: 2000
    });
}

$(".detail-slide__mains").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  asNavFor: ".detail-slide__subs"
});

$(".detail-slide__subs").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: ".detail-slide__mains",
  dots: false,
  arrows: true,
  focusOnSelect: true
});

$(".detail-infor-quality__up").click(function() {
  let inputVal = $(".detail-infor-quality__input").val();
  $(this)
    .closest(".detail-infor-quality__row")
    .find(".detail-infor-quality__input")
    .val(parseInt(inputVal) + 1);
});

$(".detail-infor-quality__down").click(function() {
  let inputVal = $(".detail-infor-quality__input").val();
  $(this)
    .closest(".detail-infor-quality__row")
    .find(".detail-infor-quality__input")
    .val(parseInt(inputVal) - 1);
});

$(".header-login__tab").click(function() {
  var tab_id = $(this).attr("data-tab");

  $(".header-login__tab").removeClass("header-login__tab--active");
  $(".header-login__content").removeClass("header-login__content--active");

  $(this).addClass("header-login__tab--active");
  $("#" + tab_id).addClass("header-login__content--active");
});

$(".header-inner__account--not-login").click(function() {
  $(".header-login").addClass("active");
  $(".header-login__overlay").addClass("active");
});

$(".header-login__close").click(function() {
  $(".header-login").removeClass("active");
  $(".header-login__overlay").removeClass("active");
});

$(".header-login__overlay").click(function() {
  $(".header-login").removeClass("active");
  $(this).removeClass("active");
});

$(".header-inner__account--loged").click(function() {
  $(".header-inner-sub-account").toggleClass("active");
});

$(".order-history-search__input-date").datepicker();

if (window.outerWidth < 768) {
  $(".order-history-list__item").click(function() {
    if ($(this).hasClass("order-history-list__item--active")) {
      $(this)
        .find(".order-history-list__content")
        .slideUp();
      $(this).removeClass("order-history-list__item--active");
    } else {
      $(".order-history-list__item").removeClass(
        "order-history-list__item--active"
      );
      $(".order-history-list__content").slideUp();
      $(this)
        .find(".order-history-list__content")
        .slideDown();
      $(this).addClass("order-history-list__item--active");
    }
  });
} else {
  var height = $(".order-history-right--active").outerHeight();
  $(".order-history__inner").css("height", height);
  $(".order-history-list__item").click(function() {
    let dataId = $(this).attr("data-id");
    $(".order-history-list__item").removeClass(
      "order-history-list__item--active"
    );
    $(".order-history-right").removeClass("order-history-right--active");
    $(this).addClass("order-history-list__item--active");
    $("#" + dataId).addClass("order-history-right--active");

    var height = $("#" + dataId).outerHeight();
    $(".order-history__inner").css("height", height);
  });
}

$(".collection-banner__items").slick({
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  variableWidth: true
});

$(".collection-content__img").imagesCompare();

// var item = null;
// var maxHeight = 0;
// $(".partner__item").each(function() {
//   var height = $(this).height();
//   if (height > maxHeight) {
//     maxHeight = height;
//     item = $(this);
//   }
// });

// $('.partner__item').css('height', maxHeight);
